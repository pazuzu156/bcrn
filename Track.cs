namespace bcrn {
  public struct Track {
    /// <summary>
    /// Track Artist Name.
    /// </summary>
    public string Artist;
    /// <summary>
    /// Track Album Name.
    /// </summary>
    public string Album;
    /// <summary>
    /// Track Name.
    /// </summary>
    public string Name;
    /// <summary>
    /// Track Number.
    /// </summary>
    public uint Position;
  }
}
