﻿using System.IO.Compression;
using System.Text.RegularExpressions;

namespace bcrn {
  public class Program {
    public static void Main(string[] args) {
      if (Directory.Exists("./zips") && Directory.GetFiles("./zips").Count() > 0) {
        foreach (string fileString in Directory.GetFiles("./zips")) {
          Directory.CreateDirectory("./tmp");
          Directory.CreateDirectory("./Music");
          FileStream fs = File.Open(fileString, FileMode.Open);
          fs.Close();
          ZipArchive zip = ZipFile.OpenRead(fs.Name);
          Track track = new Track{ Artist = "", Album = "" };
          bool confirmed = false;
          string[] covers = {"cover.jpg", "cover.jpeg", "cover.png"};
          foreach (ZipArchiveEntry entry in zip.Entries) {
            if (!covers.Any(entry.Name.Contains)) {
              entry.ExtractToFile($"./tmp/{entry.Name}", true);
              var tFile = TagLib.File.Create($"./tmp/{entry.Name}");
              var ext = Path.GetExtension(entry.Name);
              if (track.Artist == "" & track.Album == "") {
                track = new Track{
                  Name = tFile.Tag.Title,
                  Position = tFile.Tag.Track,
                  Artist = tFile.Tag.FirstAlbumArtist,
                  Album = tFile.Tag.Album
                };
              } else {
                track.Name = tFile.Tag.Title;
                track.Position = tFile.Tag.Track;
              }

              if (!confirmed) {
                Console.WriteLine("Let's make sure the tag info is correct. Press ENTER to skip if correct, otherwise, supply the correction");
                Console.WriteLine("Zip file: " + Path.GetFileName(fs.Name));
                Console.Write($"Artist: [{track.Artist}] ");
                var artist = Console.ReadLine();
                Console.Write($"Album: [{track.Album}] ");
                var album = Console.ReadLine();
                track.Artist = (artist != "") ? artist : track.Artist;
                track.Album = (album != "") ? album : track.Album;
                confirmed = true;
                Console.WriteLine();
              }

              tFile.Tag.Album = track.Album;
              tFile.Tag.AlbumArtists = new string[] { track.Artist };
              tFile.Save();

              string MoveTo = $"./Music/{track.Artist}/{Regex.Replace(track.Album, "[\":/]", "")}";
              Directory.CreateDirectory(MoveTo);
              File.Move(
                $"./tmp/{entry.Name}",
                $"{MoveTo}/{track.Position.ToString("D2")} - {Regex.Replace(track.Name, "[\":/]", "")}{ext}",
                true
              );
            } else {
              if (track.Album != null) {
                entry.ExtractToFile($"./Music/{track.Artist}/{Regex.Replace(track.Album, "[\":/]", "")}/cover.jpg", true);
              }
            }
          }
          zip.Dispose();
          Directory.Delete("./tmp", true);
        }

        Console.WriteLine();
        Console.Write("Would you like to remove the zip files to clean up space? [Y/n] ");
        string r = Console.ReadLine();
        if (r.ToLower() == "y") {
          foreach (string f in Directory.GetFiles("./zips")) {
            File.Delete(f);
          }
        }
      } else {
        Console.WriteLine("You need a folder called \"zips\" and it needs the zip files downloaded from Bandcamp.\nIf you didn't have one made, a folder has been made for you.\nSimply place your zip files in that folder and re-run bcrn.");
        Directory.CreateDirectory("./zips");
        Console.WriteLine();
        Console.Write("Press any key to continue...");
        Console.ReadKey();
      }
    }
  }
}
